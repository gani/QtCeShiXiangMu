﻿#include <QCoreApplication>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <QSettings>
#include <QTextCodec>
#include <QDateTime>

/*
 * QJsonArray：数组对象
 * QJsonObject：Josn 对象
 * QJsonDocument：json 转换对象，用于存 json 文件
 */

/*
 * 实验内容：
 * 1. QJsonObject 对象插入数据
 * 2. QJsonObject 对象插入已存在相同数据
 * 3. QJsonObject 对象插入 key 相同、value 不同数据
 * 4. QJsonObject 对象查询数据
 * 5. 删除 QJsonObject 对象中已有对象
 * 6. QJsonArray 插入数据
 * 7. QJsonArray 插入重复数据
 * 8. QJsonArray 查询数据
 * 9. QJsonArray 修改数据
 * 10. QJsonObject 插入 QJsonArray
 */

void jsonObj()
{
    QJsonObject obj;
    // 1. 插入数据
    obj.insert("test1", "this is test1");
    qDebug() << obj << "\n";

    // 2. 插入已存在相同数据
    obj.insert("test1", "this is test1");
    qDebug() << obj << "\n";

    // 3. 插入 key 相同、value 不同数据
    obj.insert("test1", "test1");
    qDebug() << obj << "\n";

    // 4. 查询数据
    obj.insert("test2", "test2");
    obj.insert("test3", "test3");
    obj.insert("test4", "test4");
    obj.insert("test5", "test5");
    qDebug() << obj.constFind("test2")->toString() << "\n";

    // 5. 删除数据
    obj.remove("test2");
    qDebug() << obj << "\n";

    // 6. QJsonArray 插入数据
    QJsonArray arr;
    arr.push_back(QJsonValue("arr1"));
    arr.push_back(QJsonValue("arr2"));
    arr.push_back(QJsonValue("arr3"));
    arr.push_back(QJsonValue("arr4"));
    qDebug() << arr << "\n";

    // 7. QJsonArray 插入重复数据
    arr.push_back(QJsonValue("arr1"));
    qDebug() << arr << "\n";

    // 8. QJsonArray 查询数据
    if(arr.contains(QJsonValue("arr2"))) {
        qDebug() << "arr2 is exist" << "\n";
    }

    // 9. QJsonArray 修改数据
    arr.replace(1, QJsonValue("arr replace"));
    qDebug() << arr << "\n";

    // 10. QJsonObject 插入 QJsonArray
    obj.insert("arr", arr);
    qDebug() << obj << "\n";

    // 10. QJsonObject 重复插入 QJsonArray
    arr.replace(1, QJsonValue("arr2"));
    obj.insert("arr", arr);
    qDebug() << obj << "\n";
}

/*
 * static 成员对象测试
 * static 对象成员往往想当做全局变量来使用，测试内容为 static 对象在多个内中使用
 */
class Test;


class Test{
public:
    Test() { a = 5; /*qDebug() << "Test";*/ }
    void setA(int a) { this->a = a; }
    int getA() const { return a; }
private:
    int a;
};

class Test1 {
public:
    Test1(){ qDebug() << "Test1"; t->setA(6); t->getA(); }
private:
    static Test* t;
};

class Test2 {
public:
    Test2(){ qDebug() << "Test2";/* t->getA();*/ }
private:
    static Test* t;
};

Test* Test1::t = new Test;
Test* Test2::t = nullptr;



/*
 * QSetting 读取中文乱码问题
*/

void iniTest()
{
    QString name = "F:/QuakeCollectLocate/build-JMainFrame-Desktop_Qt_5_13_2_MSVC2017_32bit-Debug/XZMK.prj";
    QSettings setting(name, QSettings::IniFormat);
    setting.setIniCodec("UTF-8");
    QString pName = setting.value("PROJECT/ProjectName").toString();
    QString date = setting.value("PROJECT/Date").toString();
    QString path = setting.value("PROJECT/FilePath").toString();
    qDebug() << pName;
    qDebug() << date;
    qDebug() << path;
}

void datetimeFormat()
{
    QString temp = QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss");
    qDebug() << temp;
    QDateTime dt = QDateTime::fromString(temp, "yyyy-MM-dd_hh-mm-ss");
    temp = dt.toString("yyyy-MM-dd hh:mm:ss");
    qDebug() << temp;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    datetimeFormat();


//    iniTest();

//    jsonObj();

    /*
    Test1 t1;
    Test2 t2;

    qDebug() << "end";
*/
    return a.exec();
}
