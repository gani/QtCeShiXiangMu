#ifndef CHLISTDLG_H
#define CHLISTDLG_H

#include <QDialog>
#include <QTableWidget>

namespace Ui {
class ChListDlg;
}

class ChListDlg : public QDialog
{
    Q_OBJECT

public:
    explicit ChListDlg(QMap<int, QString> chInfo, QWidget *parent = 0);
    ~ChListDlg();

    void show();

private slots:
    void on_tableWidget_doubleClicked(const QModelIndex &index);

public:
    int index;
private:
    Ui::ChListDlg *ui;
};

#endif // CHLISTDLG_H
