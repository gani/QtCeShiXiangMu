#include "complex.h"

Complex::Complex(double r, double i)
    : m_real(r)
    , m_image(i)
{

}

Complex::Complex(const Complex &c)
{
    this->m_real = c.real();
    this->m_image = c.image();
}

double Complex::real() const
{
    return m_real;
}

double Complex::image() const
{
    return m_image;
}


void Complex::setReal(double r)
{
    this->m_real = r;
}

void Complex::setImage(double i)
{
    this->m_image = i;
}
