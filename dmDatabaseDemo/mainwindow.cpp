#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QDate>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    resize(450, 300);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    qInfo() << QSqlDatabase::drivers();
    QString name = "DSN=dmdb;server=localhost;TCP_PORT=5236;uid=SYSDBA;pwd=qwert123456";
    QSqlDatabase db = QSqlDatabase::addDatabase("QODBC");
    db.setDatabaseName(name);
    bool isOk = db.open();
    if(isOk)
    {
        QSqlQuery query(db);
       qDebug()<<"open right";
//       QString sql = "insert into filedata.fileinfo(time, name, path) values('2020-09-13', 'test.h5', 'C:/test.h5');";
//       query.exec(sql);
       QString sql = "select * from filedata.fileinfo;";
       query.exec(sql);
       while (query.next()) {
           QString time = query.value("time").toDate().toString("yyyy-MM-dd");
           QString name = query.value("name").toString();
           QString path = query.value("path").toString();

           qInfo() << time << ", " << name << ", " << path;
       }
    }
    else
    {
      qDebug()<<"open failed";
    }

    db.close();
}
