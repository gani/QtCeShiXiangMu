﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpacerItem>
#include <QLineEdit>
#include <QLabel>
#include "dxflib/dl_dxf.h"
#include "dxflib/dl_creationadapter.h"
#include "interactiveview.h"
#include "dxfreader.h"
#include "jlineitem.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget/*, public DL_CreationAdapter*/
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();
private:
    Ui::Widget *ui;
protected:
    QGraphicsScene* Scene;
    InteractiveView* View;
    QHBoxLayout* hLayoutXCenter;
    QHBoxLayout* hLayoutYCenter;
    QHBoxLayout *hLayoutXOffset;
    QHBoxLayout *hLayoutYOffset;
    QHBoxLayout *hLayoutZoom;
    QVBoxLayout *vLayoutParam;
    QHBoxLayout *hLayoutMain;
    qreal XOffset;
    qreal YOffset;
    qreal ZoomDelta;
    // 中心点
    qreal XCenter;
    qreal YCenter;
    QVector<QLineF> dxfLines;
    QList<DL_TextData> dxfText;
private slots:
    void on_BtnReadDXF_clicked();
    void on_XOffset_editingFinished();
    void on_YOffset_editingFinished();
    void on_Zoom_editingFinished();
    void on_EditXCenter_editingFinished();
    void on_EditYCenter_editingFinished();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
};

#endif // WIDGET_H
