#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QUdpSocket>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QMutex>

enum LogMode {
    LM_FILE,
    LM_NET,
    LM_DATABASE
};

class DatabaseInfo{
public:
    QString server;
    QString user;
    QString passwd;
    QString dbName;
    int port;
    DatabaseInfo(const QString &ser = Q_NULLPTR,
                 const QString &uid = Q_NULLPTR,
                 const QString &pwd = Q_NULLPTR,
                 const QString &name = Q_NULLPTR,
                 int p = 3306)
        : server(ser)
        , user(uid)
        , passwd(pwd)
        , dbName(name)
        , port(p) {}
};


void setLogMode(LogMode mode);
void setLogFilePath(const QString &path);
void setLogNetPort(int port);
void setLogDatabaseInfo(const DatabaseInfo &info);
void log(QtMsgType type, const QMessageLogContext &info, const QString &msg);

class Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger(QObject *parent = Q_NULLPTR);
    ~Logger();
    void setOutputMode(LogMode mode);
    void outputLog(const QString &type, const char* file, const char* func, int line, const QString &msg);

private:
    QUdpSocket      m_socket;
    QSqlDatabase    m_db;
    QMutex          m_mutex;
};

#endif // LOGGER_H
